import multiprocessing
import sys
import time

from BasicCyclon import BasicCyclon
from CommonTools import TypePeer, GraphType
from CyclonEnvironmentManager import CyclonManagerEnvironment
from EnhancedCyclon import EnhancedCyclon

STARTING_PORT_NUMBER: int = 10000


def launch_cyclon(type_launch: TypePeer, number_of_peers: int, runtime: float, plotting_cycle: int, max_neighbor: int,
                  shuffle_length: int, graph_experiment: GraphType, remote: bool, min_time_shuffle: int,
                  max_time_shuffle: int) -> None:
    """
    Launches the Cyclon protocol with the specified parameters.

    Args:
        type_launch (TypePeer): Type of Cyclon protocol (BASIC or ENHANCED).
        number_of_peers (int): Number of peers in the network.
        runtime (float): Duration of the experiment in seconds.
        plotting_cycle (int): Interval for plotting data.
        max_neighbor (int): Maximum number of neighbors per peer.
        shuffle_length (int): Length of the shuffle operation.
        graph_experiment (GraphType): Type of graph experiment (REGULAR or SMALL_WORLD).
        remote (bool): Indicates if running in remote mode.
        min_time_shuffle (int): Minimum time between shuffle operations.
        max_time_shuffle (int): Maximum time between shuffle operations.
    """

    cyclon_manager_environment: CyclonManagerEnvironment = CyclonManagerEnvironment(STARTING_PORT_NUMBER,
                                                                                    plotting_cycle,
                                                                                    STARTING_PORT_NUMBER + 1000,
                                                                                    max_neighbor,
                                                                                    runtime, shuffle_length,
                                                                                    type_launch, number_of_peers,
                                                                                    remote,
                                                                                    min_time_shuffle,
                                                                                    max_time_shuffle, graph_experiment)

    processes: list[multiprocessing.Process()] = []

    port_list: list[int] = cyclon_manager_environment.list_of_ports

    dict_of_peer_neighbors: dict[int, list[int]] = cyclon_manager_environment.initial_list_neighbors

    list_of_peers: list = []

    if type_launch == TypePeer.BASIC:
        list_of_peers: list[BasicCyclon] = \
            [BasicCyclon(port, list(dict_of_peer_neighbors[port]),
                         max_neighbor, shuffle_length, min_time_shuffle,
                         max_time_shuffle) for port in port_list]

    elif type_launch == TypePeer.ENHANCED:
        list_of_peers: list[EnhancedCyclon] = \
            [EnhancedCyclon(port, list(dict_of_peer_neighbors[port]),
                            max_neighbor, shuffle_length, min_time_shuffle,
                            max_time_shuffle) for port in port_list]

    for peer in list_of_peers:
        p1: multiprocessing.Process = multiprocessing.Process(target=peer.start_cyclon)
        p1.start()
        processes.append(p1)

    starting_moment_of_experiment: float = time.perf_counter()

    p2: multiprocessing.Process = multiprocessing.Process(target=cyclon_manager_environment.start_data_collection,
                                                          args={starting_moment_of_experiment})
    p2.start()
    processes.append(p2)

    while not cyclon_manager_environment.event_stop:
        time.sleep(1)

    for process in processes:
        process.join()

    for process in processes:
        process.terminate()


def checking_max_neighbor_and_shuffle(shuffle_length, max_neighbor) -> None:
    """
        Checks if the shuffle length exceeds the maximum number of neighbors and exits if true.

        Args:
            shuffle_length (int): Length of the shuffle operation.
            max_neighbor (int): Maximum number of neighbors per peer.
        """
    if shuffle_length > max_neighbor:
        print("ERROR: SHUFFLE_LENGTH > MAX_NEIGHBOR. Exiting the program.")
        sys.exit(1)


def checking_max_min_shuffling_time(min_time, max_time) -> None:
    """
        Checks if the minimum shuffle time is greater than or equal to the maximum shuffle time and exits if true.

        Args:
            min_time (int): Minimum time between shuffle operations.
            max_time (int): Maximum time between shuffle operations.
        """
    if min_time > max_time or min_time == 0 or max_time == 0:
        print("ERROR: min > max or min == 0 or max == 0 shuffling time, Exiting the program.")
        sys.exit(1)


def collect_inputs():
    """
        Collects user inputs for the experiment parameters and returns them.

        Returns:
            tuple: Collected input values for the experiment.
        """
    remote: int = int(input("Are you on Lab's clusters ? 1 for True or 0 for False: "))
    if remote != 1 and remote != 0 or type(remote) != int:
        print("ERROR: wrong typing Lab's clusters")
        sys.exit(1)

    remote_output: bool = True
    if remote == 1:
        remote_output = True
    elif remote == 0:
        remote_output = False

    type_experiment: int = int(input("0 for Basic and 1 for Enhanced: "))
    if type_experiment != 0 and type_experiment != 1 or type(type_experiment) != int:
        print("ERROR: wrong typing Type experiment")
        sys.exit(1)

    output_type_experiment: type_experiment = TypePeer.BASIC
    if type_experiment == 0:
        output_type_experiment = TypePeer.BASIC
    elif type_experiment == 1:
        output_type_experiment = TypePeer.ENHANCED

    number_peers: int = int(input("Number of peers desired: "))
    if number_peers <= 0 or type(number_peers) != int:
        print("ERROR: wrong typing number of peers")
        sys.exit(1)

    runtime: float = float(input("Runtime of experiment(s): "))
    if runtime < 0 or type(runtime) != float:
        print("ERROR: wrong typing runtime")
        sys.exit(1)

    plotting_cycle: int = int(input("Plotting cycle, how often to collect data(s): "))
    if plotting_cycle < 0 or type(plotting_cycle) != int:
        print("ERROR: wrong typing plottingCycle")
        sys.exit(1)

    graph_type: int = int(input("Graph Type initialization \n1 for Circulant\n2 for Small world \n"))
    if graph_type != 1 and graph_type != 2 or type(graph_type) != int:
        print("ERROR: wrong typing graphType")
        sys.exit(1)

    output_graph_type: GraphType = GraphType.CIRCULANT_GRAPH

    if graph_type == 1:
        output_graph_type = GraphType.CIRCULANT_GRAPH
    elif graph_type == 2:
        output_graph_type = GraphType.SMALL_WORLD_GRAPH

    max_neighbors = int(input("Maximum allowed number of Neighbors: "))
    if max_neighbors < 0 or type(max_neighbors) != int:
        print("ERROR: wrong typing maxNeighbors")
        sys.exit(1)

    shuffle_length = int(input("ShuffleLength: "))
    if shuffle_length < 0 and type(shuffle_length) != int:
        print("ERROR: wrong typing shuffleLength")
        sys.exit(1)

    checking_max_neighbor_and_shuffle(shuffle_length, max_neighbors)

    min_time_shuffle = int(input("MinTimeShuffle: "))
    if min_time_shuffle < 0 and type(min_time_shuffle) != int:
        print("ERROR: wrong typing minTimeShuffle")
        sys.exit(1)

    max_time_shuffle = int(input("MaxTimeShuffle: "))
    if max_time_shuffle < 0 and type(max_time_shuffle) != int:
        print("ERROR: wrong typing maxTimeShuffle")
        sys.exit(1)

    return (output_type_experiment, number_peers, runtime, plotting_cycle,
            output_graph_type, max_neighbors, shuffle_length, remote_output, min_time_shuffle, max_time_shuffle)


def main() -> None:
    """
        Main function to initialize inputs, run the Cyclon experiment, and measure execution time.
        """
    start: float = time.perf_counter()

    # Initializing inputs for experiment
    (type_experiment, number_peers, runtime, plotting_cycle,
     graph_type, max_neighbors, shuffle_length, remote, min_time_shuffle, max_time_shuffle) = collect_inputs()

    # Running the experiment
    print(f"####### Launching real-world {type_experiment.value} Cyclon experiment #######")
    launch_cyclon(type_experiment, number_peers, runtime, plotting_cycle, max_neighbors, shuffle_length, graph_type,
                  remote,
                  min_time_shuffle, max_time_shuffle)

    finish: float = time.perf_counter()
    print("Finished in main", round(finish - start, 2), "second(s)")


if __name__ == "__main__":
    main()
