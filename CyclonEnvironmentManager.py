import os
import socket
import multiprocessing
import time
import pickle
import networkx as nx
import matplotlib.pyplot as plt
import numpy as np
import random
from scipy.stats import entropy as scipy_entropy
from scipy.stats import wasserstein_distance

from typing import Final
from multiprocessing import Value

from CommonTools import TypePeer, GraphType, TypeRequest, send_message

BUFFER_SIZE: int = 1024
PROBABILITY_GENERATION: float = 0.7
TIME_OUT: int = 60

SharedFloat = multiprocessing.Value

THRESHOLD_PLOTTING_CASES: Final[int] = 30


class CyclonManagerEnvironment:
    def __init__(self, start_port: int, plot_cycle: int, port: int, max_neighbor: int,
                 maximum_allowed_experiment_time: float, shuffle_length: int, type_cyclon: TypePeer,
                 number_of_peers: int, remote: bool, min_time_shuffle, max_time_shuffle, graph_experiment,
                 manual_input_graph=None):
        """
        Initializes the TopologyManager instance.

        Args:
            start_port (int): Starting port number.
            plot_cycle (int): Interval for plotting data.
            port (int): Port number for the manager.
            max_neighbor (int): Maximum number of neighbors per node.
            maximum_allowed_experiment_time (float): Maximum allowed experiment time.
            shuffle_length (int): Length of the shuffle.
            type_cyclon (TypePeer): Type of Cyclon protocol.
            number_of_peers (int): Total number of peers.
            remote (bool): Indicator if running in remote mode.
            min_time_shuffle (int): Minimum time between shuffles.
            max_time_shuffle (int): Maximum time between shuffles.
            graph_experiment (GraphType): Type of graph experiment.
            manual_input_graph (list[(int, int)], optional): List of edges for manual graph input.
        """

        self.offset_port_number: Final[int] = start_port
        self.max_neighbor: Final[int] = max_neighbor
        self.plot_cycle: Final[int] = plot_cycle
        self.port: Final[int] = port
        self.maximum_allowed_experiment_time: Final[float] = maximum_allowed_experiment_time
        self.shuffle_length: Final[int] = shuffle_length
        self.type_cyclon: Final[TypePeer] = type_cyclon
        self.total_number_of_peers: Final[int] = number_of_peers
        self.remote: Final[bool] = remote

        self.min_time_shuffle: Final[int] = min_time_shuffle
        self.max_time_shuffle: Final[int] = max_time_shuffle

        # Information for final plotting
        self.initial_graph_type: GraphType = graph_experiment
        self.probability_init: float = 0.0

        self.layout: nx.spring_layout = None
        self.list_of_ports: list[int] = []
        self.initial_list_neighbors: dict[int: list[int]] = {}
        self.starting_moment_experiment: SharedFloat = Value('d', 0.0)

        manager: multiprocessing.Manager = multiprocessing.Manager()

        self.event_stop: multiprocessing.Event = manager.Event()

        self.variance_frequency = Value('B', 0)
        self.variance_frequency = pickle.dumps({})

        self.entropy_frequency = Value('B', 0)
        self.entropy_frequency = pickle.dumps({})

        self.min_and_max_frequency = Value('B', 0)
        self.min_and_max_frequency = pickle.dumps({})

        self.kl_divergence = Value('B', 0)
        self.kl_divergence = pickle.dumps({})

        self.emd_distance = Value('B', 0)
        self.emd_distance = pickle.dumps({})

        self.node_frequency_over_time = Value('B', 0)
        self.node_frequency_over_time = pickle.dumps({})

        self.total_updates_over_time = Value('B', 0)
        self.total_updates_over_time = pickle.dumps({})

        self.lock = multiprocessing.Lock()

        self.graph_builder(graph_experiment, list_edges=manual_input_graph)

    def graph_builder(self, graph_type: GraphType, list_edges: list[(int, int)] = None,
                      probability: float = PROBABILITY_GENERATION) -> None:
        """
        Builds the initial graph based on the specified type and parameters.

        Args:
            graph_type (GraphType): Type of graph to build.
            list_edges (list[(int, int)], optional): List of edges for manual graph input.
            probability (float): Probability for edge rewiring in small world graph.
        """

        graph_temp: nx.DiGraph = nx.DiGraph()

        # ----------------------------------
        # ---------Generating graph---------
        if graph_type == GraphType.CIRCULANT_GRAPH:
            graph_temp: nx.DiGraph = self.generate_circulant_graph(self.total_number_of_peers)

        elif graph_type == GraphType.SMALL_WORLD_GRAPH:

            graph_temp: nx.DiGraph = self.generate_circulant_graph(self.total_number_of_peers)

            # Rewire edges with input probability
            edges: list[(int, int)] = list(graph_temp.edges())
            nodes: set[int] = set(range(self.total_number_of_peers))
            for (u, v) in edges:
                if random.random() < probability:
                    potential_targets: set[int] = set(nodes) - set(graph_temp.successors(u)) - {u}
                    if len(potential_targets) > 0:
                        new_v: int = random.choice(list(potential_targets))
                        graph_temp.remove_edge(u, v)
                        graph_temp.add_edge(u, new_v)

            graph_temp: nx.DiGraph = self.connectivity_and_max_neighbor_check(graph_temp, self.max_neighbor)

        elif graph_type == GraphType.MANUAL_GRAPH:
            graph_temp.add_edges_from(list_edges)

        # ------------------------------------------------------------
        # ---------Initializing peers once a graph is created---------
        list_of_nodes: list[int] = graph_temp.nodes()
        self.list_of_ports: list[int] = [node + self.offset_port_number for node in list_of_nodes]

        list_of_neighbors_nodes: list[list[int]] = [graph_temp.successors(node) for node in list_of_nodes]
        list_of_neighbors_ports: list[list[int]] = [[node + self.offset_port_number for node in list_nodes]
                                                    for list_nodes in list_of_neighbors_nodes]

        self.initial_list_neighbors: dict[int: list[int]] = dict(zip(self.list_of_ports, list_of_neighbors_ports))
        self.layout: nx.spring_layout = nx.circular_layout(graph_temp)

        # Saving information for final plotting
        self.initial_graph_type: GraphType = graph_type
        self.probability_init: float = probability

        self.plot_initial_topology()

    def plot_initial_topology(self) -> None:
        """
        Plots the initial topology of the graph if the number of peers is below the threshold and not in remote mode.
        """
        if self.total_number_of_peers <= THRESHOLD_PLOTTING_CASES and not self.remote:

            current_node_topology = self.peer_into_node_dict_converter(self.initial_list_neighbors)

            # Creating the new topology graph
            graph: nx.DiGraph = nx.DiGraph()
            for current_node, current_list_node in current_node_topology.items():
                for node in current_list_node:
                    graph.add_edge(current_node, node)

            fig, axe1 = plt.subplots(figsize=(5, 5))
            nx.draw(graph, self.layout, ax=axe1, with_labels=True, node_size=500, node_color="lightblue",
                    arrowsize=20)
            axe1.set_title(f"Initial topology", fontsize='large')
            plt.tight_layout()
            plt.show()

    def data_collector(self) -> None:
        """
        Collects data from all peers, processes the data, and plots the current state of the topology.
        """

        current_topology: dict[int: list[int]] = {}
        dict_of_reached_peers: dict[int: list[int]] = {}
        dict_update_counter_of_peer: dict[int: int] = {}
        dict_of_dict_frequency: dict[int: dict[int: int]] = {}
        dict_of_dict_of_ages: dict[int: dict[int: int]] = {}

        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:
            sock.bind(('localhost', self.port))

            sock.setblocking(True)
            sock.settimeout(TIME_OUT)
            current_moment: float = time.perf_counter()
            current_collecting_moment: float = round(current_moment - self.starting_moment_experiment, 2)
            print(f"------------- Request data at {current_collecting_moment}(s) moment  -------------")

            start_time_data_collection = time.time()
            for peer in self.list_of_ports:
                message_to_send: (list[int], list[int], int, TypeRequest) = ([], self.port, TypeRequest.TYPE_DATA)
                send_message(message_to_send, peer)

                try:
                    data, _ = sock.recvfrom(BUFFER_SIZE)

                    data_unpickled: (list[int], list[int], int, dict[int: int], dict[int: int]) = pickle.loads(data)
                    current_list_of_neighbors: list[int] = data_unpickled[0]
                    neighbors_seen: list[int] = data_unpickled[1]
                    update_counter: int = data_unpickled[2]
                    dict_frequency_by_peer: dict[int: int] = data_unpickled[3]
                    if self.type_cyclon == TypePeer.ENHANCED:
                        dict_age_by_peer: dict[int: int] = data_unpickled[4]

                    current_topology[peer] = current_list_of_neighbors
                    dict_of_reached_peers[peer] = neighbors_seen
                    dict_update_counter_of_peer[peer] = update_counter
                    dict_of_dict_frequency[peer] = dict_frequency_by_peer
                    if self.type_cyclon == TypePeer.ENHANCED:
                        dict_of_dict_of_ages[peer] = dict_age_by_peer

                except socket.timeout:
                    print(f"Timeout in CyclonManager waiting for peer's response {peer} in CYCLON MANAGER class")

        end_time_data_collection = time.time()
        time_to_collect_data: float = round(end_time_data_collection - start_time_data_collection, 2)

        # ------ Printing current seen neighbors -----------
        if self.total_number_of_peers <= THRESHOLD_PLOTTING_CASES:
            print(f"Total number of peers {self.total_number_of_peers}, {time_to_collect_data}(s) to collect")
            for peer, list_peers_reached in dict_of_reached_peers.items():
                print(f"peer {peer} has reached {list_peers_reached}")

        # case big topology, plot a small sample of them
        elif self.total_number_of_peers > THRESHOLD_PLOTTING_CASES:
            print(
                f"Total number of peers {self.total_number_of_peers} plotting sample, {time_to_collect_data}(s) to collect")
            counter: int = 0
            threshold_printing: int = 10

            for peer, list_peers_reached in dict_of_reached_peers.items():
                counter += 1
                if counter > threshold_printing:
                    break
                print(f"peer {peer} has reached {list_peers_reached}")

        self.end_experiment_checker(current_collecting_moment)

        self.plot_current_data_topology(dict_of_dict_frequency, current_topology, current_collecting_moment,
                                        dict_of_reached_peers, dict_update_counter_of_peer)

    def peer_into_node_dict_converter(self, current_peer_topology) -> dict[int: list[int]]:
        """
        Converts peer ports into node identifiers by subtracting the port offset.

        Args:
            current_peer_topology (dict[int: list[int]]): Current topology of peers.

        Returns:
            dict[int: list[int]]: Converted topology with node identifiers.
        """

        current_node_topology: dict[int: list[int]] = {
            peer - self.offset_port_number: [peer - self.offset_port_number for peer in list_neighbor_peer]
            for peer, list_neighbor_peer in current_peer_topology.items()}

        return current_node_topology

    def plot_current_data_topology(self, dict_of_dict_frequency: dict[int: dict[int: int]],
                                   current_peer_topology: dict[int: list[int]],
                                   time_plotting: float = 0,
                                   dict_of_seen_peers: dict[int: list[int]] = None,
                                   dict_update_counter_of_peer: dict[int: int] = None) -> None:
        """
        Plots the current data of the topology including frequency, variance, entropy, KL divergence, and EMD.

        Args:
            dict_of_dict_frequency (dict[int: dict[int: int]]): Dictionary of frequencies by peer.
            current_peer_topology (dict[int: list[int]]): Current topology of peers.
            time_plotting (float): Current time of plotting.
            dict_of_seen_peers (dict[int: list[int]], optional): Dictionary of seen peers.
            dict_update_counter_of_peer (dict[int: int], optional): Dictionary of update counters by peer.
        """

        # -----------------------------------
        # ------Preparing data to plot-------

        current_node_topology = self.peer_into_node_dict_converter(current_peer_topology)

        dict_update_counter_of_nodes: dict[int: int] = {}
        if dict_of_seen_peers and dict_update_counter_of_peer is not None:
            dict_update_counter_of_nodes = {peer - self.offset_port_number: counter
                                            for peer, counter in dict_update_counter_of_peer.items()}

        # ----------------------------------
        # ----------Plotting data-----------

        finished_execution: bool = True if time_plotting > self.maximum_allowed_experiment_time else False

        results: dict[int: float] = {node: 0 for node in list(current_node_topology.keys())}

        total_number_seen: int = 0
        for freq_dict in dict_of_dict_frequency.values():
            for key, value in freq_dict.items():
                if key - self.offset_port_number in list(results.keys()):
                    results[key - self.offset_port_number] += value
                    total_number_seen += value

        normalized_frequencies = {}
        # Avoid zero division
        if total_number_seen > 0:
            normalized_frequencies = {node: value / total_number_seen for node, value in results.items()}

        number_of_nodes = len(normalized_frequencies)

        if number_of_nodes > 0:

            total_number_of_updates: int = 0
            for value in list(dict_update_counter_of_nodes.values()):
                total_number_of_updates += value

            mean_frequency = np.mean(list(normalized_frequencies.values()))
            fig, ((ax1, ax4), (ax2, ax3), (ax5, ax6)) = plt.subplots(3, 2, figsize=(40, 18))

            mean = 1 / number_of_nodes
            mean_array = [mean for _ in range(len(normalized_frequencies))]

            # -------------------
            # -------plot 1------
            if not self.remote or self.remote and finished_execution:
                ax1.plot(list(normalized_frequencies.keys()), list(normalized_frequencies.values()),
                         label='Normalized Frequencies', color='b', marker='o', linestyle='-')
                ax1.hlines(1 / number_of_nodes, xmin=0, xmax=max(normalized_frequencies.keys()), colors='r',
                           linestyles='solid',
                           label=f"mean = 1/({self.total_number_of_peers}) = {round(mean_frequency, 3)}")
                ax1.set_xlabel('Peers', fontsize='large')
                ax1.set_ylabel('Frequency values', fontsize='large')
                ax1.set_title(f'Plt 1: Peer frequencies Visualization at {time_plotting} (s),'
                              f'{total_number_of_updates} updates in total, '
                              f'{self.type_cyclon.value} Cyclon, '
                              f'initial topology {self.initial_graph_type.value}, '
                              f'shuffling periods [{self.min_time_shuffle} , {self.max_time_shuffle}]', fontsize='large')

                threshold_print: int = 20
                if self.total_number_of_peers < threshold_print:
                    ax1.set_xticks(list(normalized_frequencies.keys()))
                elif self.total_number_of_peers >= threshold_print:
                    num_labels = threshold_print
                    x_label = np.linspace(0, max(normalized_frequencies.keys()), num=num_labels, dtype=int)
                    x_label = np.unique(
                        np.append(x_label, [0, max(normalized_frequencies.keys())]))
                    x_label = np.sort(x_label)
                    ax1.set_xticks(x_label)
                    ax1.set_xticklabels(x_label, rotation=45, ha='right')

                ax1.legend()
                ax1.grid(True)

            # -------------------
            # -------plot 2------
            with self.lock:
                min_and_max_value = pickle.loads(self.min_and_max_frequency)
                current_list = list(normalized_frequencies.values())
                current_list.sort()
                min_value = current_list[0]
                max_value = current_list[-1]
                second_lowest_value = current_list[1] if len(current_list) > 1 else None
                second_highest_value = current_list[-2] if len(current_list) > 1 else None
                min_and_max_value[time_plotting] = (
                    min_value, max_value, second_lowest_value, second_highest_value)
                self.min_and_max_frequency = pickle.dumps(min_and_max_value)

            if not self.remote or self.remote and finished_execution:
                times = list(min_and_max_value.keys())
                min_values = [value[0] for value in min_and_max_value.values()]
                max_values = [value[1] for value in min_and_max_value.values()]
                second_lowest_values = [value[2] for value in min_and_max_value.values()]
                second_highest_values = [value[3] for value in min_and_max_value.values()]

                # Plotting the min and max values
                ax2.plot(times, min_values, linestyle='-', color='blue', label='Min Frequency')
                ax2.plot(times, max_values, linestyle='-', color='blue', label='Max Frequency')

                # Plotting the second and third-lowest points
                ax2.plot(times, second_lowest_values, linestyle='-', color='green', label='2nd Lowest Frequency')

                # Plotting the second and third-highest points
                ax2.plot(times, second_highest_values, linestyle='-', color='green', label='2nd Highest Frequency')

                ax2.hlines(y=mean_frequency, xmin=min(times), xmax=max(times), colors='r', linestyles='-',
                           label=f"mean = 1/({self.total_number_of_peers}) = {round(mean_frequency, 3)}")
                # Fill between the min and max curves
                ax2.fill_between(times, min_values, max_values, color='royalblue', alpha=0.3)
                ax2.set_xlabel('Time in seconds', fontsize=14)
                ax2.set_ylabel('Frequency Values', fontsize=14)
                ax2.set_title(f'Plt 2: Min and Max Frequency Values Over Time, '
                              f'{total_number_of_updates} updates in total, '
                              f'{self.type_cyclon.value} Cyclon, '
                              f'initial topology {self.initial_graph_type.value}, '
                              f'shuffling periods [{self.min_time_shuffle} , {self.max_time_shuffle}]', fontsize=16)
                ax2.legend()
                ax2.grid(True)

            # -------------------
            # -------plot 3------
            with self.lock:
                node_frequency_over_time = pickle.loads(self.node_frequency_over_time)
                node_frequency_over_time[time_plotting] = normalized_frequencies
                self.node_frequency_over_time = pickle.dumps(node_frequency_over_time)

            with self.lock:
                total_updates_over_time = pickle.loads(self.total_updates_over_time)
                total_updates_over_time[time_plotting] = total_number_of_updates
                self.total_updates_over_time = pickle.dumps(total_updates_over_time)

            if not self.remote or self.remote and finished_execution:
                max_plots = 4
                sorted_times = sorted(node_frequency_over_time.keys())

                num_nodes = []
                for i, curr_time in enumerate(sorted_times):
                    if i >= max_plots:
                        break

                    sorted_frequencies = sorted(node_frequency_over_time[curr_time].values(), reverse=True)
                    num_nodes = range(len(sorted_frequencies))
                    total_updates = total_updates_over_time[curr_time]

                    ax3.plot(num_nodes, sorted_frequencies, linestyle='-', marker='o',
                             label=f'Taken at {curr_time}(s), ({total_updates} updates)')

                ax3.hlines(1 / number_of_nodes, xmin=0, xmax=max(normalized_frequencies.keys()), colors='r',
                           linestyles='solid',
                           label=f"mean = 1/({self.total_number_of_peers}) = {round(mean_frequency, 3)}")

                ax3.set_xlabel('Peers', fontsize='large')
                ax3.set_ylabel('Frequency values', fontsize='large')
                ax3.set_title(f'Plt 3: Peer Frequencies sorted Over Time, '
                              f'{self.type_cyclon.value} Cyclon\n '
                              f'initial topology {self.initial_graph_type.value}, '
                              f'shuffling periods [{self.min_time_shuffle} , {self.max_time_shuffle}]', fontsize='large')

                # Adjust x-axis tick labels for large numbers of nodes
                if self.total_number_of_peers > 20:
                    tick_indices = np.linspace(0, self.total_number_of_peers - 1, 20, dtype=int)
                    ax3.set_xticks(tick_indices)
                    ax3.set_xticklabels([num_nodes[i] for i in tick_indices])

                ax3.legend()
                ax3.grid(True)

            # -------------------
            # -------plot 4------
            with self.lock:
                variance_frequency = pickle.loads(self.variance_frequency)
                variance_frequency[time_plotting] = np.var(list(normalized_frequencies.values()))
                self.variance_frequency = pickle.dumps(variance_frequency)

            with self.lock:
                entropy_frequency = pickle.loads(self.entropy_frequency)
                entropy_frequency[time_plotting] = -scipy_entropy(list(normalized_frequencies.values()), base=2)
                self.entropy_frequency = pickle.dumps(entropy_frequency)

            if not self.remote or self.remote and finished_execution:
                ax4.plot(list(variance_frequency.keys()), list(variance_frequency.values()), marker='o',
                         linestyle='-',
                         color='c', label='Variance')
                ax4.set_xlabel('Time in seconds', fontsize='large')
                ax4.set_ylabel('Variance', fontsize='large')
                ax4.set_title(f'Plt 4: Entropy vs Variance Over Time, '
                              f'{total_number_of_updates} updates in total, '
                              f'{self.type_cyclon.value} Cyclon, '
                              f'initial topology {self.initial_graph_type.value}, '
                              f'shuffling periods [{self.min_time_shuffle} , {self.max_time_shuffle}]', fontsize='large')
                ax4.grid(True)

                ax4b = ax4.twinx()
                ax4b.plot(list(entropy_frequency.keys()), list(entropy_frequency.values()), marker='o', linestyle='-',
                          color='g', label='Entropy')
                ax4b.set_ylabel('Entropy', fontsize='large')

                lines, labels = ax4.get_legend_handles_labels()
                lines2, labels2 = ax4b.get_legend_handles_labels()
                ax4b.legend(lines + lines2, labels + labels2, loc='upper left', fontsize=16)

            # -------------------
            # -------plot 5------
            with self.lock:
                kl_divergence_dict = pickle.loads(self.kl_divergence)
                kl_divergence_dict[time_plotting] = scipy_entropy(list(normalized_frequencies.values()), mean_array,
                                                                  base=2)
                self.kl_divergence = pickle.dumps(kl_divergence_dict)

            if not self.remote or self.remote and finished_execution:
                ax5.plot(list(kl_divergence_dict.keys()), list(kl_divergence_dict.values()), marker='o',
                         linestyle='-',
                         color='orange', label='Relative entropy')
                ax5.set_xlabel('Time in seconds', fontsize='large')
                ax5.set_ylabel('Relative Entropy', fontsize='large')
                ax5.set_title(f'Plt 5: Relative Entropy vs Variance Over Time, '
                              f'{total_number_of_updates} updates in total, '
                              f'{self.type_cyclon.value} Cyclon, '
                              f'initial topology {self.initial_graph_type.value}, '
                              f'shuffling periods [{self.min_time_shuffle} , {self.max_time_shuffle}]', fontsize='large')
                ax5.grid(True)

                ax5b = ax5.twinx()
                ax5b.plot(list(variance_frequency.keys()), list(variance_frequency.values()), marker='o', linestyle='-',
                          color='g', label='Variance')
                ax5b.set_ylabel('Variance', fontsize='large')

            # -------------------
            # -------plot 6------
            with self.lock:
                emd_dict = pickle.loads(self.emd_distance)
                emd_dict[time_plotting] = \
                    self.earth_mover_distance(list(normalized_frequencies.values()), mean_array)
                self.emd_distance = pickle.dumps(emd_dict)
            if not self.remote or self.remote and finished_execution:
                ax6.plot(list(emd_dict.keys()), list(emd_dict.values()), marker='o', linestyle='-', color='purple',
                         label='EMD')
                ax6.set_xlabel('Time in seconds', fontsize='large')
                ax6.set_ylabel('Earth Mover\'s Distance', fontsize='large')
                ax6.set_title('Plt 6: Earth Mover\'s Distance Over Time', fontsize='large')
                ax6.grid(True)

                plt.tight_layout()

            if not self.remote:
                plt.show()
            plt.close()

            self.saving_data_when_experiment_terminated(time_plotting, fig, normalized_frequencies, variance_frequency,
                                                        entropy_frequency, kl_divergence_dict, emd_dict)

    def saving_data_when_experiment_terminated(self, time_plotting: float, fig, normalized_results: dict[int: float],
                                               variance_frequency: dict[float: float],
                                               entropy_frequency: dict[float: float],
                                               kl_divergence_dict: dict[float: float], emd_dict: dict[float: float]):
        """
        Saves the experiment data and plots when the experiment has terminated.

        Args:
            time_plotting (float): Current time of plotting.
            fig: Figure object of the plot.
            normalized_results (dict[int: float]): Normalized frequency results.
            variance_frequency (dict[float: float]): Variance over time.
            entropy_frequency (dict[float: float]): Entropy over time.
            kl_divergence_dict (dict[float: float]): KL divergence over time.
            emd_dict (dict[float: float]): Earth Mover's Distance over time.
        """

        # -------------------------------------------------
        # --------Save when execution terminated----------
        if time_plotting > self.maximum_allowed_experiment_time:

            name = (
                f"FinalType-numNeigh_{self.max_neighbor}-runtime_{self.maximum_allowed_experiment_time}-"
                f"shuffle_{self.shuffle_length}-type_{self.type_cyclon.value}-"
                f"numPeers_{self.total_number_of_peers}-initialGen_{self.initial_graph_type.value}-"
                f"minTimeShuffle_{self.min_time_shuffle}-maxTimeShuffle_{self.max_time_shuffle}.png"
            )

            if self.remote:
                directory = "/home/hawazine/BachelorProject/plots"
                list_directory = "/home/hawazine/BachelorProject/saveExecutionLists.txt"
            else:
                directory = "C:\\Users\\ilyas\\OneDrive\\Desktop\\bachelor_project\\BachelorProject\\savePlots"
                list_directory = "SaveListExecution.txt"

            # Saving graphical plots
            plots_dir = os.path.join(directory, name)
            fig.savefig(plots_dir)
            plt.close(fig)

            with open(list_directory, 'a') as file:
                file.write(f"\n############START DATA COLLECTION##############\n")
                file.write(f"# Execution type : {name}\n\n")
                file.write(f"# Frequencies at the end :{normalized_results}\n\n")

                file.write(f"# variance over time :{variance_frequency}\n\n")
                file.write(f"# entropy over time :{entropy_frequency}\n\n")
                file.write(f"# kl_divergence over time :{kl_divergence_dict}\n\n")
                file.write(f"# emd over time :{emd_dict}\n")
                file.write(f"\n##########STOP DATA COLLECTION################\n")

    @staticmethod
    def earth_mover_distance(list1: list[float], list2: list[float]) -> float:
        """
        Compute the Earth Mover's Distance between two normalized histograms (lists of equal size).

        Parameters:
            list1 (List[float]): First histogram (already normalized).
            list2 (List[float]): Second histogram (already normalized).

        Returns:
            float: Earth Mover's Distance between hist1 and hist2.
        """
        if len(list1) != len(list2):
            raise ValueError("Arrays must have the same size.")

        return wasserstein_distance(list1, list2)

    def end_experiment_checker(self, current_time) -> None:
        """
        Checks if the experiment has exceeded the allowed maximum time and stops the experiment if necessary.

        Args:
            current_time (float): Current experiment time.
        """

        if current_time > self.maximum_allowed_experiment_time:
            print(f"Experiment has ended: the experiment"
                  f" exceeded allowed maximum time {self.maximum_allowed_experiment_time} seconds")
            self.stop_data_collection()

    def start_data_collection(self, starting_moment: float) -> None:
        """
        Starts the data collection process and periodically collects data at the specified plot cycle.

        Args:
            starting_moment (float): Starting moment of data collection.
        """

        with self.starting_moment_experiment.get_lock():
            self.starting_moment_experiment: float = starting_moment

        plot_cycle: int = self.plot_cycle
        while not self.event_stop.is_set():
            time.sleep(plot_cycle)
            self.data_collector()

    def stop_data_collection(self) -> None:
        """
        Stops the data collection process and sends a shutdown message to all peers.
        """

        # Send to all peers it's time to stop the experiment
        for peer in self.list_of_ports:
            message_to_send: (list[int], list[int], int, TypeRequest) = ([], self.port, TypeRequest.TYPE_SHUT_DOWN)
            send_message(message_to_send, peer)

        self.event_stop.set()

        end_experiment_moment: float = time.perf_counter()
        print("Finished in TopologyManager", round(end_experiment_moment - self.starting_moment_experiment, 2),
              "second(s)")

    @staticmethod
    def generate_circulant_graph(number_nodes: int) -> nx.DiGraph:
        """
        Generates a regular directed graph with the specified number of nodes.

        Args:
            number_nodes (int): Number of nodes in the graph.

        Returns:
            nx.DiGraph: Generated regular directed graph.
        """

        result_graph: nx.DiGraph = nx.DiGraph()
        out_degree = in_degree = 2  # type: int
        result_graph.add_nodes_from(range(number_nodes))

        for node in result_graph.nodes():
            # Outgoing edges
            for i in range(1, out_degree + 1):
                result_graph.add_edge(node, (node + i) % number_nodes)

            # Incoming edges (if in_degree != out_degree)
            for i in range(1, in_degree + 1):
                result_graph.add_edge((node - i) % number_nodes, node)

        return result_graph

    @staticmethod
    def connectivity_and_max_neighbor_check(graph_temp: nx.DiGraph, max_neighbor: int) -> nx.DiGraph:
        """
        Ensures full connectivity and enforces the maximum number of neighbors constraint.

        Args:
            graph_temp (nx.DiGraph): Temporary graph to check and modify.
            max_neighbor (int): Maximum number of neighbors.

        Returns:
            nx.DiGraph: Graph with ensured connectivity and max neighbor constraint.
        """

        undirected_g: nx.Graph = graph_temp.to_undirected()

        # Ensure full connectivity in the sense of undirected graph
        while not nx.is_connected(undirected_g):
            components: list[set[int]] = list(nx.connected_components(undirected_g))

            component_1: int = np.random.choice(len(components))
            component_2: int = np.random.choice(len(components))
            while component_1 == component_2:
                component_2: int = np.random.choice(len(components))

            node_from_comp1: int = np.random.choice(list(components[component_1]))
            node_from_comp2: int = np.random.choice(list(components[component_2]))

            graph_temp.add_edge(node_from_comp1, node_from_comp2)

            undirected_g: nx.Graph = graph_temp.to_undirected()

        # Enforce max_neighbor constraint on the directed graph for every node
        for node in list(graph_temp.nodes):
            neighbors: list[int] = list(graph_temp.successors(node))
            if len(neighbors) > max_neighbor:
                # Excess neighbors, decide which edges to remove
                while len(neighbors) > max_neighbor:
                    neighbor_to_remove: int = np.random.choice(neighbors)

                    graph_temp.remove_edge(node, neighbor_to_remove)
                    undirected_temp: nx.Graph = graph_temp.to_undirected()

                    if not nx.is_connected(undirected_temp):
                        graph_temp.add_edge(node, neighbor_to_remove)
                        neighbors.remove(neighbor_to_remove)
                    else:
                        neighbors: list[int] = list(graph_temp.successors(node))

        return graph_temp
