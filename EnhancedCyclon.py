import socket
import multiprocessing
import time
import random
import pickle

from typing import Final
from multiprocessing import Value
from CommonTools import send_message, TypeRequest

OFFSET_PORT: int = 100
BUFFER_SIZE: int = 1024
TIME_OUT: int = 40

# Custom type for managing proxy list with manager
manager_dict_proxy = dict[int: int]
manager_list_proxy = list[int]

shared_byte = multiprocessing.Value


class EnhancedCyclon:

    def __init__(self, port: int, first_neighbor_list: list[int], max_neighbor: int, shuffle_length: int,
                 min_time_shuffle, max_time_shuffle):
        """
        Initializes the PeerEnhancedCyclon instance with the given parameters.

        Args:
            port (int): The port number for this peer.
            first_neighbor_list (list[int]): Initial list of neighbor ports.
            max_neighbor (int): Maximum number of neighbors.
            shuffle_length (int): Number of neighbors to shuffle.
            min_time_shuffle (int): Minimum time interval between shuffles.
            max_time_shuffle (int): Maximum time interval between shuffles.
        """

        self.port: Final[int] = port
        self.max_neighbor: Final[int] = max_neighbor
        self.shuffle_length: Final[int] = shuffle_length

        self.min_time_shuffle: Final[int] = min_time_shuffle
        self.max_time_shuffle: Final[int] = max_time_shuffle

        manager: multiprocessing.Manager = multiprocessing.Manager()

        self.event_stop: multiprocessing.Event = manager.Event()

        self.current_dict_neighbors: shared_byte = Value('B', 0)
        self.current_dict_neighbors = pickle.dumps({peer: 0 for peer in first_neighbor_list})

        self.subset_sent_as_p: shared_byte = Value('B', 0)
        self.subset_sent_as_p = pickle.dumps({})

        self.lock = multiprocessing.Lock()

        # ----- For performance evaluation -----------
        # ----- Data collected for CyclonManager -----
        self.list_neighbors_reached: shared_byte = Value('B', 0)
        self.list_neighbors_reached = pickle.dumps({})

        self.update_counter: multiprocessing.Value = Value('i', 0)

        self.frequency_seen_peers: dict[int: int] = manager.dict()
        self.frequency_seen_peers = {port: 0 for port in first_neighbor_list}

    # -------------------------------------------------------------
    # -------Atomically handling the shared variables -------------

    def change_current_dict_neighbors(self, new_dict: dict[int: int]) -> None:
        """
        Args:
            new_dict (dict[int: int]): New dictionary of neighbors.
        """
        with self.lock:
            self.current_dict_neighbors = pickle.dumps(new_dict)

    def change_subset_sent_as_p(self, new_subset: list[int]) -> None:
        """
        Args:
            new_subset (list[int]): New subset of neighbors sent.
        """
        with self.lock:
            self.subset_sent_as_p = pickle.dumps(new_subset)

    def change_list_neighbors_reached(self, new_subset: list[int]) -> None:
        """
        Args:
            new_subset (list[int]): New list of neighbors reached.
        """
        with self.lock:
            self.list_neighbors_reached = pickle.dumps(new_subset)

    def get_current_dict_neighbors(self) -> dict[int: int]:
        """
        Returns:
            dict[int: int]: Current dictionary of neighbors.
        """
        with self.lock:
            return pickle.loads(self.current_dict_neighbors)

    def get_subset_sent_as_p(self) -> list[int]:
        """
        Returns:
            list[int]: Subset of neighbors sent.
        """
        with self.lock:
            return pickle.loads(self.subset_sent_as_p)

    def get_list_neighbors_reached(self) -> list[int]:
        """
        Returns:
            list[int]: List of neighbors reached.
        """
        with self.lock:
            return pickle.loads(self.list_neighbors_reached)

    # --------------- End atomicity handling ---------------
    # ------------------------------------------------------

    def shuffle_and_send(self) -> None:
        """
        Selects a random subset of neighbors, sends a shuffle request to the oldest neighbor,
        and updates the subset of neighbors sent.
        """

        current_dict: dict[int: int] = self.get_current_dict_neighbors()

        if len(current_dict) > 0:
            new_current_dict_neighbor: dict[int: int] = {peer: age + 1 for peer, age in current_dict.items()}

            oldest_peer: int = max(new_current_dict_neighbor, key=new_current_dict_neighbor.get)

            random_keys: list[int] = random.sample(list(new_current_dict_neighbor.keys() - {oldest_peer}),
                                                   min(self.shuffle_length - 1, len(new_current_dict_neighbor) - 1))

            # Append oldest_peer for implementation purposes, it will be discarded in self.update_my_neighbor_list
            subset_to_send_as_p: dict[int: int] = {oldest_peer: new_current_dict_neighbor[oldest_peer]}
            subset_to_send_as_p.update({key: new_current_dict_neighbor[key] for key in random_keys})

            subset_to_send_as_p[self.port] = 0

            self.change_subset_sent_as_p(list(subset_to_send_as_p.keys()))
            self.change_current_dict_neighbors(new_current_dict_neighbor)

            # Type of the message communicated between peers : (dict[int: int], int, TypeRequest)
            message_to_send = (subset_to_send_as_p, self.port, TypeRequest.TYPE_SHUFFLE)
            send_message(message=message_to_send, to_port=oldest_peer)

    def message_listener(self) -> None:
        """
        Listens for incoming messages, processes them based on their type, and updates the neighbor list
        accordingly. Handles different message types such as TYPE_SHUFFLE, TYPE_ANSWER, TYPE_DATA, and TYPE_SHUT_DOWN.
        """

        with (socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock):

            sock.bind(('localhost', self.port))
            sock.setblocking(True)
            sock.settimeout(TIME_OUT)

            # Type of the message communicated between peers : (dict[int: int], int, TypeRequest)
            # Type of message communicated between a peer and CyclonManager :
            #       (list[int], list[int], int, dict[int: int], dict[int: int])
            while not self.event_stop.is_set():

                try:
                    data, _ = sock.recvfrom(BUFFER_SIZE)

                    # ------------------------- Handling received data --------------------------------------------------
                    # Form of the message (received_subset_from_sender_node, my initial_list sent ,sender_node, "type request")

                    data_unpickled: (dict[int: int], int, TypeRequest) = pickle.loads(data)

                    subset_received: dict[int: int] = data_unpickled[0]
                    sender_peer: int = data_unpickled[1]
                    type_request: TypeRequest = data_unpickled[2]

                    # ------------------------------------------------------------------
                    # ------- Case P received its asked set of neighbors ---------------
                    if type_request == TypeRequest.TYPE_ANSWER:

                        subset_sent: list[int] = self.get_subset_sent_as_p()
                        self.update_my_neighbor_list(subset_received, subset_sent)

                        empty_subset: list[int] = []
                        self.change_subset_sent_as_p(empty_subset)

                    # ------------------------------------------------------------------
                    # ------- Case Q received a request to exchange its neighbors ------
                    elif type_request == TypeRequest.TYPE_SHUFFLE:
                        current_dict: dict[int: int] = self.get_current_dict_neighbors()

                        if len(current_dict) > 0:
                            random_keys: list[int] = random.sample(list(current_dict.keys()),
                                                                   min(self.shuffle_length, len(current_dict)))

                            subset_to_send_as_q: dict[int: int] = {key: current_dict[key] for key in random_keys}

                            message_to_send = (subset_to_send_as_q, self.port, TypeRequest.TYPE_ANSWER)
                            send_message(message_to_send, sender_peer)

                            self.update_my_neighbor_list(subset_received, list(subset_to_send_as_q.keys()))

                    # ---------------------------------------------------------------------
                    # ------- Case Cyclon Manager asks to send current collected Data -----
                    elif type_request == TypeRequest.TYPE_DATA:

                        with self.update_counter.get_lock():
                            current_update_counter: int = self.update_counter.value

                        current_dict: dict[int: int] = self.get_current_dict_neighbors()

                        current_list_of_reached_neighbors: list[int] = self.get_list_neighbors_reached()

                        message_to_send = (list(current_dict.keys()), list(current_list_of_reached_neighbors),
                                           current_update_counter, self.frequency_seen_peers, current_dict)
                        send_message(message_to_send, sender_peer)

                    # ---------------------------------------------------------------------
                    # ------- Case experiment is over, this node shall be killed ----------
                    elif type_request == type_request.TYPE_SHUT_DOWN:
                        self.stop_cyclon()
                        break

                except socket.timeout:
                    print(f"Timeout in peer {self.port} waiting for a peer's response in ENHANCED algorithm")

            self.stop_cyclon()

    def update_my_neighbor_list(self, subset_received: dict[int: int], subset_sent: list[int]) -> None:
        """
        Updates the current dictionary of neighbors based on the received subset and the subset sent.
        Ensures no duplicates and maintains the maximum number of neighbors.

        Args:
            subset_received (dict[int: int]): Dictionary of neighbors received from another peer.
            subset_sent (list[int]): List of neighbors sent to another peer.
        """

        current_dict_of_neighbors: dict[int: int] = self.get_current_dict_neighbors()

        subset_received_cleaned = {peer: age for peer, age in subset_received.items()
                                   if peer not in list(current_dict_of_neighbors)
                                   and peer != self.port}

        subset_sent_cleaned = [peer for peer in subset_sent if peer != self.port]

        # Remove entries sent to Q from the current dictionary
        current_dict_of_neighbors: dict[int: int] = {peer: age for peer, age in current_dict_of_neighbors.items()
                                                     if peer not in subset_sent_cleaned}

        # Sort the current neighbors by age (oldest first)
        sorted_neighbors = sorted(current_dict_of_neighbors.items(), key=lambda x: x[1], reverse=True)

        # Add new neighbors from the received subset
        for new_peer, age in subset_received_cleaned.items():
            if len(current_dict_of_neighbors) < self.max_neighbor:
                current_dict_of_neighbors[new_peer] = age
            else:
                # Replace the oldest entry in the cache with the new entry
                oldest_peer, _ = sorted_neighbors.pop()
                del current_dict_of_neighbors[oldest_peer]
                current_dict_of_neighbors[new_peer] = age

        self.change_current_dict_neighbors(current_dict_of_neighbors)

        # ----- For performance evaluation -----------
        # ----- Data collected for CyclonManager -----
        with self.update_counter.get_lock():
            self.update_counter.value += 1

        old_list_reached_neigh = self.get_list_neighbors_reached()

        new_list_reached_neigh: list[int] = list(
            set(list(current_dict_of_neighbors.keys()) + list(old_list_reached_neigh)))

        self.change_list_neighbors_reached(new_list_reached_neigh)

        for port in list(current_dict_of_neighbors.keys()):
            if port in self.frequency_seen_peers.keys():
                self.frequency_seen_peers[port] += 1
            else:
                self.frequency_seen_peers[port] = 0

    def start_cyclon(self) -> None:
        """
        Starts the Cyclon protocol, initiating the message listener as a separate process and
        periodically performing shuffle operations.
        """

        # Initiating the communication listener as a separate process for this peer
        listener_process: multiprocessing.Process = multiprocessing.Process(target=self.message_listener)
        listener_process.start()

        while not self.event_stop.is_set():
            time_to_sleep: int = random.randint(self.min_time_shuffle, self.max_time_shuffle)
            time.sleep(time_to_sleep)
            self.shuffle_and_send()

        listener_process.terminate()

    def stop_cyclon(self) -> None:
        """
        Stops the Cyclon protocol by setting the event flag to terminate ongoing operations.
        """
        self.event_stop.set()
