import pickle
from enum import Enum
import socket


class TypeRequest(Enum):
    """
    Enum for different types of requests in the Cyclon protocol.
    """
    TYPE_SHUFFLE: str = "shuffle_request"
    TYPE_ANSWER: str = "answer_request"
    TYPE_DATA: str = "data_collection"
    TYPE_SHUT_DOWN: str = "stop_experiment"


class GraphType(Enum):
    """
    Enum for different types of graphs used in the Cyclon protocol.
    """
    CIRCULANT_GRAPH: str = "circulant_graph"
    SMALL_WORLD_GRAPH: str = "small_world_graph"
    MANUAL_GRAPH: str = "manual_graph"


class TypePeer(Enum):
    """
    Enum for different types of Cyclon peers.
    """
    BASIC: str = "basic"
    ENHANCED: str = "enhanced"


def send_message(message, to_port: int) -> None:
    """
    Sends a serialized message to the specified port using UDP.

    Args:
        message: The message to be sent.
        to_port (int): The port number to send the message to.
    """
    message_to_send = pickle.dumps(message)
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:
        server_address: (str, int) = ('localhost', to_port)
        sock.sendto(message_to_send, server_address)
