import socket
import multiprocessing
import time
import random
import pickle

from typing import Final
from multiprocessing import Value

from CommonTools import TypeRequest, send_message


BUFFER_SIZE: int = 1024
TIMEOUT: int = 40

# Custom type for managing proxy list with manager
Manager_list_proxy = list[int]


class BasicCyclon:

    def __init__(self, port: int, first_neighbor_list: list[int], max_neighbor: int, shuffle_length: int,
                 min_time_shuffle: int, max_time_shuffle: int):
        """
        Initializes the PeerBasicCyclon instance with the given parameters.

        Args:
            port (int): The port number for this peer.
            first_neighbor_list (list[int]): Initial list of neighbor ports.
            max_neighbor (int): Maximum number of neighbors.
            shuffle_length (int): Number of neighbors to shuffle.
            min_time_shuffle (int): Minimum time interval between shuffles.
            max_time_shuffle (int): Maximum time interval between shuffles.
        """

        self.port: Final[int] = port
        self.max_neighbor: Final[int] = max_neighbor
        self.shuffle_length: Final[int] = shuffle_length

        self.min_time_shuffle: Final[int] = min_time_shuffle
        self.max_time_shuffle: Final[int] = max_time_shuffle

        manager: multiprocessing.Manager = multiprocessing.Manager()

        self.event_stop: multiprocessing.Event = manager.Event()
        self.current_list_neighbors: Manager_list_proxy = manager.list(first_neighbor_list)
        self.subset_sent_as_p: Manager_list_proxy = manager.list()

        # ----- For performance evaluation -----------
        # ----- Data collected for CyclonManager -----
        self.list_neighbors_reached: Manager_list_proxy = manager.list(first_neighbor_list)
        self.update_counter: multiprocessing.Value = Value('i', 0)
        self.frequency_seen_peers: dict[int: int] = {port: 0 for port in first_neighbor_list}

    def shuffle_and_send(self) -> None:
        """
        Selects a random subset of neighbors, sends a shuffle request to a randomly chosen neighbor,
        and updates the subset of neighbors sent.
        """

        if len(self.current_list_neighbors) > 0:
            subset_to_send_as_p: list[int] = random.sample(list(self.current_list_neighbors),
                                                           min(self.shuffle_length, len(list(self.current_list_neighbors))))

            q_node: int = random.choice(subset_to_send_as_p)

            subset_to_send_as_p.remove(q_node)
            subset_to_send_as_p.append(self.port)
            self.subset_sent_as_p: Manager_list_proxy = subset_to_send_as_p

            self.subset_sent_as_p.append(q_node)
            # Type of the message communicated between peers : (list[int], int, TypeRequest)
            message_to_send = (subset_to_send_as_p, self.port, TypeRequest.TYPE_SHUFFLE)

            send_message(message_to_send, q_node)

    def message_listener(self) -> None:
        """
        Listens for incoming messages, processes them based on their type, and updates the neighbor list
        accordingly. Handles different message types such as TYPE_SHUFFLE, TYPE_ANSWER, TYPE_DATA, and TYPE_SHUT_DOWN.
        """

        with (socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock):

            sock.bind(('localhost', self.port))
            sock.setblocking(True)
            sock.settimeout(TIMEOUT)

            # Type of the message communicated between peers : (list[int], int, TypeRequest)
            # Type of message communicated between a peer and CyclonManager : (list[int], list[int], int, dict[int: int])
            while not self.event_stop.is_set():
                try:
                    data, _ = sock.recvfrom(BUFFER_SIZE)

                    # ------------------------- Handling received data --------------------------------------------------
                    # Form of the message (received_subset_from_sender_node, my initial_list sent ,sender_node, "type request")
                    data_unpickled: (list[int], int, TypeRequest) = pickle.loads(data)

                    subset_received: list[int] = data_unpickled[0]
                    sender_peer: int = data_unpickled[1]
                    type_request: TypeRequest = data_unpickled[2]

                    # ------------------------------------------------------------------
                    # ------- Case P received its asked set of neighbors ---------------
                    if type_request == TypeRequest.TYPE_ANSWER:
                        self.update_my_neighbor_list(subset_received, list(self.subset_sent_as_p))
                        self.subset_sent_as_p: Manager_list_proxy = []

                    # ------------------------------------------------------------------
                    # ------- Case Q received a request to exchange its neighbors ------
                    elif type_request == TypeRequest.TYPE_SHUFFLE:
                        subset_to_send_as_q: list[int] = random.sample(list(self.current_list_neighbors),
                                                                       min(self.shuffle_length, len(list(self.current_list_neighbors))))

                        message_to_send = (subset_to_send_as_q, self.port, TypeRequest.TYPE_ANSWER)
                        send_message(message_to_send, sender_peer)

                        self.update_my_neighbor_list(subset_received, subset_to_send_as_q)

                    # ---------------------------------------------------------------------
                    # ------- Case Cyclon Manager asks to send current collected Data -----
                    elif type_request == TypeRequest.TYPE_DATA:
                        with self.update_counter.get_lock():
                            current_update_counter: int = self.update_counter.value

                        message_to_send = (list(self.current_list_neighbors), list(self.list_neighbors_reached),
                                           current_update_counter, self.frequency_seen_peers)
                        send_message(message_to_send, sender_peer)

                    # ---------------------------------------------------------------------
                    # ------- Case experiment is over, this node shall be killed ----------
                    elif type_request == type_request.TYPE_SHUT_DOWN:
                        self.stop_cyclon()
                        break

                except socket.timeout:
                    print(f"Timeout in peer {self.port} waiting for other peer's response, in BASIC algorithm")

            self.stop_cyclon()

    def update_my_neighbor_list(self, subset_received: list[int], subset_sent: list[int]) -> None:
        """
        Updates the current list of neighbors based on the received subset and the subset sent.
        Ensures no duplicates and maintains the maximum number of neighbors.

        Args:
            subset_received (list[int]): List of neighbors received from another peer.
            subset_sent (list[int]): List of neighbors sent to another peer.
        """

        if len(subset_received) > 0:
            subset_received: list[int] = [peer for peer in subset_received
                                          if peer not in list(self.current_list_neighbors) and peer != self.port]

            subset_sent: list[int] = [peer for peer in subset_sent if peer != self.port]

            for new_peer in subset_received:
                if len(list(self.current_list_neighbors)) <= self.max_neighbor:
                    self.current_list_neighbors.append(new_peer)
                elif len(list(self.current_list_neighbors)) > self.max_neighbor and len(subset_sent) > 0:
                    old_peer = subset_sent.pop(0)
                    self.current_list_neighbors.remove(old_peer)
                    self.current_list_neighbors.append(new_peer)

            self.current_list_neighbors: Manager_list_proxy = list(set([peer for peer in self.current_list_neighbors
                                                                        if peer not in subset_sent]))

            # ---------- For performance evaluation ---------------
            # ---------- Data collected for CyclonManager ---------
            with self.update_counter.get_lock():
                self.update_counter.value += 1

            self.list_neighbors_reached: Manager_list_proxy = list(set(list(self.current_list_neighbors)
                                                                       + list(self.list_neighbors_reached)))
            for port in self.current_list_neighbors:
                if port in self.frequency_seen_peers.keys():
                    self.frequency_seen_peers[port] += 1
                else:
                    self.frequency_seen_peers[port] = 0

    def start_cyclon(self) -> None:
        """
        Starts the Cyclon protocol, initiating the message listener as a separate process and
        periodically performing shuffle operations.
        """

        # Initiating the communication listener as a separate process for this peer
        listener_process: multiprocessing.Process = multiprocessing.Process(target=self.message_listener)
        listener_process.start()

        while not self.event_stop.is_set():
            time_to_sleep: int = random.randint(self.min_time_shuffle, self.max_time_shuffle)
            time.sleep(time_to_sleep)
            self.shuffle_and_send()

        listener_process.terminate()

    def stop_cyclon(self) -> None:
        """
        Stops the Cyclon protocol by setting the event flag to terminate ongoing operations.
        """
        self.event_stop.set()
