 
# Implementing and evaluating real-world behaviour of peer sampling Cyclon algorithm by Ilyas Hawazine

## Overview
This project implements and evaluates the real-world behavior of the Cyclon peer sampling algorithm, focusing on both the basic and enhanced protocols. The Cyclon algorithm is crucial for decentralized systems, enabling peers in a network to periodically exchange information efficiently.

## Files and Structure
The project consists of several Python scripts and classes, each playing a specific role in the implementation and evaluation of the Cyclon algorithm:

- **BasicCyclon.py**: Manages the behavior of each peer following the Basic Cyclon protocol.
- **EnhancedCyclon.py**: Manages the behavior of each peer following the Enhanced Cyclon protocol.
- **CyclonEnvironmentManager.py**: Initializes the topology and collects data during experiments.
- **MainLauncher.py**: Orchestrates the parallel launching of all peers and manages the experiment's parameters.
- **CommonTools.py**: Contains common functions used across various parts of the project.

## Implementation Details
1. **Communication**: Peers use the UDP protocol for rapid and efficient data transmission.
2. **Parallelism**: The `multiprocessing` library in Python is utilized to allow true parallelism by giving each peer its own Global Interpreter Lock (GIL).
3. **Cyclon Algorithm**:
   - **Basic Cyclon**: Peers periodically shuffle and exchange their neighbors randomly.
   - **Enhanced Cyclon**: Peers track the age of their neighbors, preferring to exchange older ones to ensure a fresher view of the network.

## Experiments and Evaluation
The experiments evaluate the performance of both Basic and Enhanced Cyclon protocols using various mathematical metrics, such as variance and earth mover’s distance. These metrics assess how well the peer frequencies are distributed and stabilized over time.

### Key Findings
- Mathematical metrics confirmed the better performance of the Enhanced algorithm, especially in managing peer ages and ensuring a more fair spread of the frequencies of seen neighbours.
- Linearly computed metrics to evaluate the frequencies of seen peers, such as the Variance, Entropy and Relative entropy, lead to similar results.
## Running the Project
If running the project from the romote clusters, use the `MainLauncher.py` script:
```bash
python -m <your_directory>.MainLauncher
```
If on your machine, you can simply launcher MainLauncher.py on your favorite IDE.

You will be prompted to input parameters such as the number of peers, runtime, initial topology, shuffling length, and shuffling period interval.

## Conclusion
This project demonstrates the implementation of the Cyclon peer sampling algorithm and provides insights into its real-world behavior. The Enhanced Cyclon algorithm proves to be more efficient and fair in distributing peer information across a network.

For more detailed information, refer to the report.

## Authors
- Supervisee Ilyas Hawazine
- Project's Professor Anne-Marie Kermarrec
- Project supervisors: Sayan Biswas, Martijn de Vos, and Rishi Sharma.

## Scalable Computing Systems Lab (SaCS)
## License
- This project is licensed under the MIT License. See the LICENSE file for details.
- This project is subject to SaCS usage.